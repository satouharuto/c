/*
	Sample.h
	Sampleclass　クラスを宣言 
*/

//クラスを宣言
class SampleClass
{
	//メンバ関数
	int a;
	int b;
	int c;

	//メンバ関数
public:
	void Input();
	void Plus();
	void Disp();

};