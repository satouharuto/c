/*
	CircleClass.h
	CircleClass.h クラスを宣言
*/

class CircleClass
{
	//メンバ関数
	int cin;
	int area;
	int r;

public:
	void Input();
	void Calc();
	void Disp();
};